(function() {

  // Controller

  var MailboxController = Backbone.Marionette.Controller.extend({
    initialize: function() {
      this.receivedMails = new ReceivedMails();
    },

    listReceived: function() {
      mailbox.contentRegion.show(new ReceivedMailsView({ collection: this.receivedMails }));
      this.receivedMails.fetch();
    },

    listSent: function() {},
    detailReceived: function() {}
  });

  // 受信メールItemView

  var ReceivedMailItemView = Backbone.Marionette.ItemView.extend({
    template: _.template('<div><%= title %></div>')
  });

  // 受信メールCollectionView

  var ReceivedMailsView = Backbone.Marionette.CollectionView.extend({
    childView: ReceivedMailItemView
  });

  // 受信メールCollection

  var ReceivedMails = Backbone.Collection.extend({
    url: '/received-mails'
  });

  // AppRouter

  var MailboxRouter = Backbone.Marionette.AppRouter.extend({
    controller: new MailboxController(),
    appRoutes: {
      'received': 'listReceived',
      'sent': 'listSent',
      'received/:id': 'detailReceived'
    }
  });

  // Application

  var mailbox = new Backbone.Marionette.Application();

  mailbox.addRegions({
    menuRegion: '#menu',
    contentRegion: '#content'
  });

  mailbox.addInitializer(function(options){
    new MailboxRouter();
    Backbone.history.start();
  });

  this.mailbox = mailbox;
}).call(this);
