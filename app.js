var express = require('express');
var join = require('path').join;

var app = express();

app.use(express.static(join(__dirname, 'public')));

app.set('view engine', 'ejs');
app.set('views', join(__dirname, 'views'));

app.get('/', function(req, res) {
  res.render('index');
});

app.get('/received-mails', function(req, res) {
  res.send([
    { id: 1, title: 'hello, world1' },
    { id: 2, title: 'hello, world2' },
    { id: 3, title: 'hello, world3' }
  ]);
});

app.listen(3000, function() {
  console.log('listening on 3000');
});
